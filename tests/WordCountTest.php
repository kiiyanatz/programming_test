<?php

use PHPUnit\Framework\TestCase;
use \App\WordCount;

class WordCountTest extends TestCase
{
  public function testWordCountInstanceCreation()
  {

    $fileWordCount = new \App\WordCount('app/files/one.txt');
    $this->assertInstanceOf(\App\WordCount::class, $fileWordCount);
  }

  public function testWordCountGetFrequencies()
  {
    $fileWordCount = new \App\WordCount('app/files/one.txt');
    $this->assertInternalType('array', $fileWordCount->getFrequecies());
  }

  public function testTotalWordCount()
  {
    $fileWordCount = new \App\WordCount('app/files/one.txt');
    $this->assertInternalType('int', $fileWordCount->getTotalWords());
  }
}
