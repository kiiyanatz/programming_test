<?php

use PHPUnit\Framework\TestCase;
use \App\FileRank;

class FileRankTest extends TestCase
{
  public function testFileRankInstanceCreation()
  {

    $file = new \App\FileRank('app/files/one.txt');
    $this->assertInstanceOf(\App\FileRank::class, $file);
  }

  public function testGetRankingType()
  {
    $file = new \App\FileRank('app/files/one.txt');
    $this->assertInternalType('array', $file->getRanking());
  }

  public function testGetRankingDataStructure()
  {
    $file = new \App\FileRank('app/files/one.txt');
    $this->assertArrayHasKey('wordfrequency', $file->getRanking());
  }

  public function testFileWordCount()
  {
    $fileOne = new \App\FileRank('app/files/one.txt');
    $fileTwo = new \App\FileRank('app/files/two.txt');
    $fileThree = new \App\FileRank('app/files/three.txt');

    $fileOneRanking = $fileOne->getRanking();
    $this->assertEquals(404, $fileOneRanking['wordcount']);

    $fileTwoRanking = $fileTwo->getRanking();
    $this->assertEquals(1151, $fileTwoRanking['wordcount']);

    $fileThreeRanking = $fileThree->getRanking();
    $this->assertEquals(953, $fileThreeRanking['wordcount']);
  }

}
