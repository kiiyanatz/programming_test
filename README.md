# README #

Follow the instructions below to run the application.

# How to install
```sh
$ git clone https://kiiyanatz@bitbucket.org/kiiyanatz/programming_test.git
$ cd programming_test
$ composer install
```

# Run application
```sh
$ php -S localhost:8000
```

# Run Tests
```sh
$ ./vendor/bin/phpunit
```
# Reports
Reports are found in the output folder named by their respective source text files. ie. one.json two.json three.json
