<html>
    <head>
        <title> Learning PHP </title>
        <style>
            .container {
                width: 80%;
                margin: 0 auto;
                background: #eee;
                height: 95vh;
                overflow: hidden;
                padding: 10px;
            }

            .box-combined {
                display: flex;
                margin-top: 1px;
                height: 47%;
            }

            .combined-info {
                flex: 1;
                overflow-y: scroll;
                background: #fff;
            }

            .analysis-summary {
                flex: 1;
            }

            .content {
                display: flex;
            }


            .box{
                flex: 1;
                overflow-y: scroll;
                height: 50%;
                position: relative;
                background: #fff;
                margin: 5px;
                padding: 10px;
            }

            .file-details ul {
                padding: 0px;
            }

            .file-details ul li {
                display: inline-block;
            }

            table {
                width: 100%;
            }
            td {
                text-align: center;
                vertical-align: middle;
            }
            .nav-div {
                background: #3a3a3a;
                height: 20px;
                font-size: 13px;
                text-align: center;
                vertical-align: middle;
                padding: 10px;
                color: #fff;
                font-family: monospace;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="box">
                    <div class="nav-div">
                        File: one.txt word ranking | Word count: <?= $fileOneRanking['wordcount']; ?>
                    </div>

                    <table>
                        <thead>
                            <th>Word</th>
                            <th>Frequency</th>
                        </thead>
                        <tbody>
                            <?php foreach($fileOneRanking['wordfrequency'] as $key => $value): ?>
                                <tr>
                                    <td> <?= $key; ?> </td>
                                    <td> <?= $value; ?> </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="box">
                    <div class="nav-div">
                        File: two.txt word ranking | Word count: <?= $fileTwoRanking['wordcount']; ?>
                    </div>

                    <table>
                        <thead>
                            <th>Word</th>
                            <th>Frequency</th>
                        </thead>
                        <tbody>
                            <?php foreach($fileTwoRanking['wordfrequency'] as $key => $value): ?>
                                <tr>
                                    <td> <?= $key; ?> </td>
                                    <td> <?= $value; ?> </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="box">
                    <div class="nav-div">
                        File: three.txt word ranking | Word count: <?= $fileThreeRanking['wordcount']; ?>
                    </div>

                    <table>
                        <thead>
                            <th>Word</th>
                            <th>Frequency</th>
                        </thead>
                        <tbody>
                            <?php foreach($fileThreeRanking['wordfrequency'] as $key => $value): ?>
                                <tr>
                                    <td> <?= $key; ?> </td>
                                    <td> <?= $value; ?> </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="box-combined">
                <div class="combined-info">
                    <div class="nav-div">
                        Word ranking from 3 combined files.
                    </div>
                     <table>
                        <thead>
                            <th>Word</th>
                            <th>Frequency</th>
                        </thead>
                        <tbody>
                            <?php foreach($mergedFrequencies as $key => $value): ?>
                                <tr>
                                    <td> <?= $key; ?> </td>
                                    <td> <?= $value; ?> </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="analysis-summary">
                     <div class="nav-div" style="text-transform: uppercase; font-size: 20;">
                        Analysis Summary
                    </div>
                    <table>
                        <thead>
                            <th>Item</th>
                            <th>Value</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>one.txt</td>
                                <td><?= $fileOneRanking['wordcount']; ?> words</td>
                            </tr>
                             <tr>
                                <td>two.txt</td>
                                <td><?= $fileTwoRanking['wordcount']; ?> words</td>
                            </tr>
                            <tr>
                                <td>three.txt</td>
                                <td><?= $fileThreeRanking['wordcount']; ?> words</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
