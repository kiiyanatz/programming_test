<?php

namespace App;

require 'WordCount.php';


class FileRank
{
  private $filename;

  function __construct($filename)
  {
    $this->filename = $filename;
  }

  public function getRanking()
  {
    $fileWordCount = new WordCount($this->filename);
    $fileRanking['wordfrequency'] = $fileWordCount->getFrequecies();
    $fileRanking['wordcount'] = $fileWordCount->getTotalWords();
    return $fileRanking;
  }

}

function saveToJsonFile($filename, $ranking)
{
  $fp = fopen('output/' .$filename, 'w');
  fwrite($fp, json_encode($ranking));
  fclose($fp);
}

$fileOne = new FileRank('app/files/one.txt');
$fileOneRanking = $fileOne->getRanking();
saveToJsonFile('one.json', $fileOneRanking);

$fileTwo = new FileRank('app/files/two.txt');
$fileTwoRanking = $fileTwo->getRanking();
saveToJsonFile('two.json', $fileTwoRanking);

$fileThree = new FileRank('app/files/three.txt');
$fileThreeRanking = $fileThree->getRanking();
saveToJsonFile('three.json', $fileThreeRanking);


$mergedWordFrequencies = array_merge_recursive($fileOneRanking['wordfrequency'], $fileTwoRanking['wordfrequency'], $fileThreeRanking['wordfrequency']);

$mergedFrequencies = [];

foreach($mergedWordFrequencies as $key => $value)
{

    if(gettype($value) == 'array')
    {
        $value = array_sum($value);
    }
    arsort($mergedFrequencies);
    $mergedFrequencies += array($key => $value);
}

saveToJsonFile('combined_report.json', $mergedFrequencies);
