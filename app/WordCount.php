<?php

namespace App;

class WordCount {
    private $words;
    private $totalWords;


    function __construct($filename)
    {
        $file_content = file_get_contents($filename);
        $this->totalWords = count(preg_split('/\s+/', $file_content));
        $this->words = (array_count_values(preg_split('/\s+/', $file_content)));
    }

    public function getFrequecies()
    {
        $wordFrequencies = [];
        foreach ($this->words as $key => $val)
        {
            $wordFrequencies[$key] = $val;
        }
        arsort($wordFrequencies);
        return $wordFrequencies;
    }

    public function getTotalWords()
    {
        return $this->totalWords;
    }
}
